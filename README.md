Notification (Incoming Call + Charge Status)
=================================================

Articles Used
--------------
* [Notification](https://developer.android.com/guide/topics/ui/notifiers/notifications) - A notification is a message that Android displays outside your app's UI to provide the user with reminders, communication from other people, or other timely information from your app.
* [IncomingCall](https://stackoverflow.com/questions/15563921/how-to-detect-incoming-calls-in-an-android-device)

![image](screen1.png)![image](screen2.png)