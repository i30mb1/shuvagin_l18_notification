package com.example.shuvagin_l17_broadcastreceiver

import android.Manifest.permission.READ_CALL_LOG
import android.Manifest.permission.READ_PHONE_STATE
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.BatteryManager
import android.os.Build
import android.os.Bundle
import android.telephony.PhoneStateListener
import android.telephony.TelephonyManager
import android.text.SpannableString
import android.text.SpannableStringBuilder
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.core.text.color
import androidx.core.text.toSpannable
import androidx.core.text.underline
import androidx.databinding.DataBindingUtil
import com.example.shuvagin_l17_broadcastreceiver.databinding.ItemReceiverBinding
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    private var isCharging: Boolean = false
    private val myPhoneStateListener = MyPhoneStateListener()
    private val br: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Intent.ACTION_BATTERY_CHANGED -> {
                    val status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1)
                    val currentState: Boolean = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL
                    if (isCharging != currentState) {
                        if (!isCharging) addEntry(getString(R.string.main_activity_plugged)) else addEntry(getString(R.string.main_activity_unplugged))
                        isCharging = currentState
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        askPermission()
        createNotificationChannel()
    }

    override fun onResume() {
        super.onResume()
        regBatteryStatus()
        regTelephoneManager()
    }

    override fun onPause() {
        unregisterReceiver(br)
        unregTelephoneManager()
        clearAllNotification()
        super.onPause()
    }

    private fun clearAllNotification() {
        with(NotificationManagerCompat.from(this)) {
            cancelAll()
        }
    }

    private fun sendNotification(contentText: String) {
        val channelID = getString(R.string.main_activity_channel_id)
        val notificationID = 101

        val notification = NotificationCompat.Builder(this, channelID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Notification")
            .setContentText(contentText)
            .setTimeoutAfter(2000)
//            .setVibrate(longArrayOf(100,200,300,400,500,400,300,200,100))
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        with(NotificationManagerCompat.from(this)) {
            notify(notificationID, notification.build())
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.main_activity_channel_name)
            val description = getString(R.string.main_activity_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(getString(R.string.main_activity_channel_id), name, importance)
            channel.description = description
            channel.enableLights(true)
            channel.lightColor = Color.BLUE
//            channel.enableVibration(true)
//            channel.vibrationPattern = longArrayOf(100,200,300,400,500,400,300,200,100)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(channel)
        }
    }

    private fun regBatteryStatus() {
        val batteryStatus = IntentFilter().apply {
            addAction(Intent.ACTION_BATTERY_CHANGED)
        }
        registerReceiver(br, batteryStatus)
    }

    private fun regTelephoneManager() {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE)
    }

    private fun unregTelephoneManager() {
        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        telephonyManager.listen(myPhoneStateListener, PhoneStateListener.LISTEN_NONE)
    }

    private fun askPermission() {
        if (ContextCompat.checkSelfPermission(this, READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(READ_CALL_LOG, READ_PHONE_STATE), 0)
        }
    }

    private fun addEntry(status: String) {
        val layout: ItemReceiverBinding = DataBindingUtil.inflate(
            layoutInflater,
            R.layout.item_receiver,
            root_activity_main,
            true
        )
        layout.tvItemReceiverMessage.text =  SpannableStringBuilder().underline {  append(status) }

        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val minutes = calendar.get(Calendar.MINUTE)
        val hours = calendar.get(Calendar.HOUR_OF_DAY)

//        layout.tvItemReceiverTime.text = String.format("%d:%d", hours, minutes)
        layout.tvItemReceiverTime.text = SpannableStringBuilder(hours.toString())
            .color(ContextCompat.getColor(this, R.color.colorAccent)) { append(":") }
            .append(minutes.toString())

        sendNotification(status)
    }

    inner class MyPhoneStateListener : PhoneStateListener() {
        override fun onCallStateChanged(state: Int, incomingNumber: String) {
            super.onCallStateChanged(state, incomingNumber)
            if (incomingNumber.isNotEmpty() && state == TelephonyManager.CALL_STATE_RINGING) {
                addEntry(getString(R.string.main_activity_incoming_number, incomingNumber))
            }
        }
    }
}
